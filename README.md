# Template Website in Hugo

Use `hugo server` to run the website locally in http://localhost:1313/.

If you want to run this on your network, specify `--bind` for your local ip, `--port` for the desired port. On a public server, use `--baseUrl` with http/https.

`hugo server --baseUrl http://myserver.example --bind 10.10.10.1 --port 4040`

### Development
Read about the shared theme at [kde-hugo wiki](https://invent.kde.org/websites/aether-sass/-/wikis/Hugo)

### I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n)
